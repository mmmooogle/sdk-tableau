#!/bin/sh

echo ">> Downloading..."
curl -o /tmp/Tableau-SDK-Linux-64Bit-10-1-2.tar.gz "https://bitbucket.org/mmmooogle/sdk-tableau/raw/78d4fa06496be49852f89d960a64011ca974b0d0/Tableau-SDK-Linux-64Bit-10-1-2.tar.gz"
echo ">> Unpacking..."
tar -xvf /tmp/Tableau-SDK-Linux-64Bit-10-1-2.tar.gz -C /tmp
cd /tmp/tableausdk-linux64-10100.16.1213.1135
mv lib64 lib
echo ">> Installing"
cp -R * /usr/local/
echo ">> Linking"
echo /usr/local/lib/tableausdk/ > /tmp/tableau.conf
sudo mv /tmp/tableau.conf /etc/ld.so.conf.d/tableau.conf
sudo ldconfig
echo ">> Done"